package com.example.clientdemo.Fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.clientdemo.Adapters.SliderPagerAdapter;
import com.example.clientdemo.Fragments.TabFragments.CompanyFragment;
import com.example.clientdemo.Fragments.TabFragments.ConsultancyFragment;
import com.example.clientdemo.Fragments.TabFragments.TrainingFragment;
import com.example.clientdemo.R;
import com.example.clientdemo.Models.Slide;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {
    private List<Slide> lstSlides ;
    private ViewPager sliderpager;


    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    FrameLayout container;

    TabLayout tabLayout;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        sliderpager = view.findViewById(R.id.slider_pager) ;


        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(),3000,4000);

        // prepare a list of slides ..
        lstSlides = new ArrayList<>() ;
        lstSlides.add(new Slide(R.drawable.logo1));
        lstSlides.add(new Slide(R.drawable.logo2));
        lstSlides.add(new Slide(R.drawable.logo3));
        lstSlides.add(new Slide(R.drawable.logo4));
        lstSlides.add(new Slide(R.drawable.logo5));
        SliderPagerAdapter adapter = new SliderPagerAdapter(getContext(),lstSlides);
        sliderpager.setAdapter(adapter);

        tabLayout = view.findViewById(R.id.tabLayout);

        container = view.findViewById(R.id.container);
        fragmentManager = getChildFragmentManager();

        fragmentTransaction = fragmentManager.beginTransaction();


        if (container != null) {

            if (savedInstanceState == null) {

                fragmentTransaction.add(R.id.container, new CompanyFragment(), null).commit();


            }


        }


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment fragment = null;

                switch (tab.getPosition()) {

                    case 0:
                        fragment = new CompanyFragment();
                        break;

                    case 1:
                        fragment = new TrainingFragment();
                        break;

                    case 2:
                        fragment = new ConsultancyFragment();

                        break;


                }
                fragmentManager.beginTransaction().replace(R.id.container, fragment, null).commit();


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        return view;
    }



    class SliderTimer extends TimerTask {
        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (sliderpager.getCurrentItem() < lstSlides.size() - 1) {
                            sliderpager.setCurrentItem(sliderpager.getCurrentItem() + 1);
                        } else {
                            sliderpager.setCurrentItem(0);
                        }
                    }
                });
            }
        }
    }
}
