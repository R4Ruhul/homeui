package com.example.clientdemo.ProfileFragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clientdemo.Adapters.CoursesAdapter;
import com.example.clientdemo.Adapters.EventsAdapter;
import com.example.clientdemo.Models.Courses;
import com.example.clientdemo.Models.Events;
import com.example.clientdemo.R;

import java.util.ArrayList;

public class ProEventFragment extends Fragment {
    RecyclerView recyclerView;
    EventsAdapter adapter;
    ArrayList<Events> list;

    public ProEventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_pro_event, container, false);

        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new EventsAdapter(getContext(),list);
        recyclerView.setAdapter(adapter);

        return  view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        list.add(new Events("BSc Level 6 Orientation","Central it club","Sun,25/08/2020 \n 9.00-10.30 AM"));
        list.add(new Events("ISO Training","Creative it ltd","Sun,29/08/2020 \n 9.00-10.30 AM"));
        list.add(new Events("Basic Android","Central it club","Mon,25/08/2020 \n 9.00-10.30 AM"));
        list.add(new Events("BSc Level 4 Dart","Divine it bd","Sun,05/08/2020 \n 9.00-10.30 AM"));


    }
}
