package com.example.clientdemo.Models;

public class Courses {
    int coursesImage;
    String coursesDetails;

    public Courses() {
    }

    public Courses(int coursesImage, String coursesDetails) {
        this.coursesImage = coursesImage;
        this.coursesDetails = coursesDetails;
    }

    public int getCoursesImage() {
        return coursesImage;
    }

    public void setCoursesImage(int coursesImage) {
        this.coursesImage = coursesImage;
    }

    public String getCoursesDetails() {
        return coursesDetails;
    }

    public void setCoursesDetails(String coursesDetails) {
        this.coursesDetails = coursesDetails;
    }
}
