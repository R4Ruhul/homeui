package com.example.clientdemo.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientdemo.R;

public class EventsViewHolder extends RecyclerView.ViewHolder {
    public  TextView eventTopic,eventPlace,eventDate;
    public EventsViewHolder(@NonNull View itemView) {
        super(itemView);
        eventTopic = itemView.findViewById(R.id.event_topic);
        eventPlace = itemView.findViewById(R.id.event_place);
        eventDate = itemView.findViewById(R.id.event_date);
    }
}
