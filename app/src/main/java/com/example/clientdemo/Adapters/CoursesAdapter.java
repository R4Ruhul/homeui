package com.example.clientdemo.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientdemo.Models.Courses;
import com.example.clientdemo.R;
import com.example.clientdemo.ViewHolder.CoursesViewHolder;

import java.util.ArrayList;

public class CoursesAdapter extends RecyclerView.Adapter<CoursesViewHolder> {
    Context context;
    ArrayList<Courses>list;

    public CoursesAdapter(Context context, ArrayList<Courses> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CoursesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_courses,parent,false);
        return new CoursesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoursesViewHolder holder, int position) {
        holder.courseDetails.setText(list.get(position).getCoursesDetails());
        holder.courseImage.setImageResource(list.get(position).getCoursesImage());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
