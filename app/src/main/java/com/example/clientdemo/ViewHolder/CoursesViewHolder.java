package com.example.clientdemo.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientdemo.R;

public class CoursesViewHolder extends RecyclerView.ViewHolder {
    public ImageView courseImage;
    public TextView courseDetails;

    public CoursesViewHolder(@NonNull View itemView) {
        super(itemView);
        courseImage = itemView.findViewById(R.id.image_view);
        courseDetails = itemView.findViewById(R.id.text_view);
    }
}
