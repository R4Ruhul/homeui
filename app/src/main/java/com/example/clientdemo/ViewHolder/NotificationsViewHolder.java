package com.example.clientdemo.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientdemo.R;

public class NotificationsViewHolder extends RecyclerView.ViewHolder {

    public TextView textHeadline,textDetails,textDate;
    public NotificationsViewHolder(@NonNull View itemView) {
        super(itemView);
        textHeadline = itemView.findViewById(R.id.text_headline);
        textDetails = itemView.findViewById(R.id.text_details);
        textDate = itemView.findViewById(R.id.text_date);
    }
}
