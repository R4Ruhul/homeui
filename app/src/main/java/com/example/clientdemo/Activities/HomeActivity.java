package com.example.clientdemo.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.clientdemo.Fragments.CoursesFragment;
import com.example.clientdemo.Fragments.HomeFragment;
import com.example.clientdemo.Fragments.LoginFragment;
import com.example.clientdemo.Fragments.OffersFragment;
import com.example.clientdemo.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    private Fragment homeFragment;
    private Fragment coursesFragment;
    private Fragment offersFragment;
    private Fragment loginFragment;
    private Fragment active;
    private FragmentManager fm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Demo Text");

        BottomNavigationView bottomNavigationView = findViewById(R.id.botton_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);


        homeFragment = new HomeFragment();
        coursesFragment = new CoursesFragment();
        offersFragment = new OffersFragment();
        loginFragment = new LoginFragment();
        fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.main_layout, loginFragment, "4").commitAllowingStateLoss();
        fm.beginTransaction().add(R.id.main_layout, offersFragment, "3").hide(loginFragment).commitAllowingStateLoss();
        fm.beginTransaction().add(R.id.main_layout, coursesFragment, "2").hide(offersFragment).commitAllowingStateLoss();
        fm.beginTransaction().add(R.id.main_layout, homeFragment, "1").hide(coursesFragment).commitAllowingStateLoss();
        active = homeFragment;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item_home:
                fm.beginTransaction().hide(active).show(homeFragment).commitAllowingStateLoss();
                active = homeFragment;
                break;
            case R.id.item_courses:
                fm.beginTransaction().hide(active).show(coursesFragment).commitAllowingStateLoss();
                active = coursesFragment;
                break;
            case R.id.item_offers:
                fm.beginTransaction().hide(active).show(offersFragment).commitAllowingStateLoss();
                active = offersFragment;
                break;
            case R.id.item_login:
                fm.beginTransaction().hide(active).show(loginFragment).commitAllowingStateLoss();
                active = loginFragment;
                break;
        }

        return false;

    }
}
