package com.example.clientdemo.Models;

public class Notifications {
    String notiHedaline;
    String notiDetails;
    String notiDate;

    public Notifications() {
    }

    public Notifications(String notiHedaline, String notiDetails, String notiDate) {
        this.notiHedaline = notiHedaline;
        this.notiDetails = notiDetails;
        this.notiDate = notiDate;
    }

    public String getNotiHedaline() {
        return notiHedaline;
    }

    public void setNotiHedaline(String notiHedaline) {
        this.notiHedaline = notiHedaline;
    }

    public String getNotiDetails() {
        return notiDetails;
    }

    public void setNotiDetails(String notiDetails) {
        this.notiDetails = notiDetails;
    }

    public String getNotiDate() {
        return notiDate;
    }

    public void setNotiDate(String notiDate) {
        this.notiDate = notiDate;
    }
}
