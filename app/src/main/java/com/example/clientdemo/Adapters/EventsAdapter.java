package com.example.clientdemo.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientdemo.Models.Courses;
import com.example.clientdemo.Models.Events;
import com.example.clientdemo.R;
import com.example.clientdemo.ViewHolder.EventsViewHolder;

import java.util.ArrayList;

public class EventsAdapter extends RecyclerView.Adapter<EventsViewHolder> {
    Context context;
    ArrayList<Events>list;

    public EventsAdapter(Context context, ArrayList<Events> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public EventsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_events,parent,false);
        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventsViewHolder holder, int position) {
        holder.eventTopic.setText(list.get(position).getEventName());
        holder.eventPlace.setText(list.get(position).getEventPlace());
        holder.eventDate.setText(list.get(position).getEventDate());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
