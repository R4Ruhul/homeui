package com.example.clientdemo.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientdemo.Models.Notifications;
import com.example.clientdemo.R;
import com.example.clientdemo.ViewHolder.NotificationsViewHolder;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsViewHolder> {
    Context context;
    ArrayList<Notifications>list;

    public NotificationsAdapter(Context context, ArrayList<Notifications> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public NotificationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_notifications,parent,false);
        return new NotificationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsViewHolder holder, int position) {
        holder.textHeadline.setText(list.get(position).getNotiHedaline());
        holder.textDetails.setText(list.get(position).getNotiDetails());
        holder.textDate.setText(list.get(position).getNotiDate());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
