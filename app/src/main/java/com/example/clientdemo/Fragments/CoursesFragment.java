package com.example.clientdemo.Fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clientdemo.Adapters.CoursesAdapter;
import com.example.clientdemo.Models.Courses;
import com.example.clientdemo.R;

import java.util.ArrayList;

public class CoursesFragment extends Fragment {

    public CoursesFragment() {

    }
    RecyclerView recyclerView;
    CoursesAdapter adapter;
    ArrayList<Courses>list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_courses, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        adapter = new CoursesAdapter(getContext(),list);
        recyclerView.setAdapter(adapter);
        return  view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        list.add(new Courses(R.drawable.logo2,"Android\n 500BDT 2 Hours"));
        list.add(new Courses(R.drawable.logo2,"Java\n 530BDT 2 Hours"));
        list.add(new Courses(R.drawable.logo2,"Kotlin\n 500BDT 2 Hours"));
        list.add(new Courses(R.drawable.logo2,"Dart\n 500BDT 2 Hours"));
        list.add(new Courses(R.drawable.logo2,"React\n 500BDT 2 Hours"));
        list.add(new Courses(R.drawable.logo2,"Flutter\n 500BDT 2 Hours"));

    }
}
