package com.example.clientdemo.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.clientdemo.Fragments.LoginFragment;
import com.example.clientdemo.ProfileFragments.ProContactFragment;
import com.example.clientdemo.ProfileFragments.ProEventFragment;
import com.example.clientdemo.ProfileFragments.ProHomeFragment;
import com.example.clientdemo.ProfileFragments.ProNotificationFragment;
import com.example.clientdemo.ProfileFragments.ProRegisterFragment;
import com.example.clientdemo.ProfileFragments.ProRewardsFragment;
import com.example.clientdemo.ProfileFragments.ProfileFragment;
import com.example.clientdemo.R;
import com.google.android.material.navigation.NavigationView;

public class ProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
     DrawerLayout drawerLayout;
     NavigationView navigationView;
     FrameLayout fragment;
     public static FragmentManager fragmentManager;
     FragmentTransaction transaction;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        fragment = findViewById(R.id.main_layout);
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();

        if (fragment != null && savedInstanceState == null) {

                transaction.add(R.id.main_layout, new ProHomeFragment(), null).commit();

        }



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar,  R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId())
        {
            case R.id.item_home:
                fragment = new ProHomeFragment();
                break;
            case R.id.item_notification:
                fragment = new ProNotificationFragment();
                break;
            case R.id.item_event:
                fragment = new ProEventFragment();
                break;
            case R.id.item_course:
                fragment = new ProRegisterFragment();
                break;
            case R.id.item_profile:
                fragment = new ProfileFragment();
                break;
            case R.id.item_contactus:
                fragment = new ProContactFragment();
                break;
            case R.id.item_rewards:
                fragment = new ProRewardsFragment();
                break;
            case R.id.item_logout:
                fragment = new LoginFragment();
                break;

        }
        fragmentManager.beginTransaction().replace(R.id.main_layout, fragment, null).commit();
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }
}
